package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;


public final class CombatReadyShip implements CombatReadyVessel {

	private DockedShip dockedShip;

	private boolean canTurn = false;

	public CombatReadyShip(DockedShip dockedShip) {
		this.dockedShip = dockedShip;
	}

	@Override
	public void endTurn() {
		this.canTurn = false;
	}

	@Override
	public void startTurn() {
		this.canTurn = true;
	}

	@Override
	public String getName() {
		return null;
	}

	@Override
	public PositiveInteger getSize() {
		return null;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return null;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.canTurn) {
			PositiveInteger damage = this.dockedShip.getAttackSubsystem().attack(target);
			NamedEntity attacker = () -> this.dockedShip.getName();
			NamedEntity weapon = () -> this.dockedShip.getAttackSubsystem().getName();
			return Optional.of(new AttackAction(damage, attacker, target, weapon));
		}
		else {
			return Optional.empty();
		}
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		return null;
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		return Optional.ofNullable(this.dockedShip.getDefenciveSubsystem().regenerate());
	}

}
