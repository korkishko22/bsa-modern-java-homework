package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor, PositiveInteger capacitorRegeneration, PositiveInteger pg, PositiveInteger speed, PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed, PositiveInteger size) {
		return DockedShipBuilder.named(name).shield(shieldHP.value()).hull(hullHP.value()).pg(powergridOutput.value())
				.capacitor(capacitorAmount.value()).capacitorRegen(capacitorRechargeRate.value()).speed(speed.value())
				.size(size.value()).construct();
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			int currentPg = this.pg.value() - subsystem.getPowerGridConsumption().value();
			if (currentPg < 0) {
				throw new InsufficientPowergridException(this.pg.value());
			}
			this.pg = PositiveInteger.of(currentPg);
			this.attackSubsystem = subsystem;
		}
		else {
			this.attackSubsystem = null;
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem != null) {
			int currentPg = this.pg.value() - subsystem.getPowerGridConsumption().value();
			if (currentPg < 0) {
				throw new InsufficientPowergridException(this.pg.value());
			}
			this.pg = PositiveInteger.of(currentPg);
			this.defenciveSubsystem = subsystem;
		}
		else {
			this.defenciveSubsystem = null;
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null) {
			if (this.defenciveSubsystem == null) {
				throw NotAllSubsystemsFitted.bothMissing();
			}
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else {
			return new CombatReadyShip(this);
		}
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public String getName() {
		return this.name;
	}

}
