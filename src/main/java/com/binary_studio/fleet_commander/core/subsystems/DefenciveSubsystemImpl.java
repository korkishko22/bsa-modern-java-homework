package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private static final double MAX_DAMAGE_REDUCTION = 0.95;

	private String name;

	private PositiveInteger impactReduction;

	private PositiveInteger shieldRegen;

	private PositiveInteger hullRegen;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public DefenciveSubsystemImpl(String name, PositiveInteger impactReduction, PositiveInteger shieldRegen,
			PositiveInteger hullRegen, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		this.name = name;
		this.impactReduction = impactReduction;
		this.shieldRegen = shieldRegen;
		this.hullRegen = hullRegen;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	public DefenciveSubsystemImpl() {
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return DefenciveSubsystemBuilder.named(name).pg(powergridConsumption.value())
				.capacitorUsage(capacitorConsumption.value()).shieldRegen(shieldRegeneration.value())
				.impactReduction(impactReductionPercent.value()).hullRegen(hullRegeneration.value()).construct();
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		int incDmg = incomingDamage.damage.value();
		double regenValue = Math.min(((double) this.impactReduction.value() / 100), MAX_DAMAGE_REDUCTION);
		PositiveInteger damage = PositiveInteger.of((int) Math.ceil((incDmg - (incDmg * regenValue))));
		NamedEntity attacker = incomingDamage.attacker;
		NamedEntity target = incomingDamage.target;
		NamedEntity weapon = incomingDamage.weapon;
		return new AttackAction(damage, attacker, target, weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegen, this.hullRegen);
	}

}
