package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger baseDamage;

	private PositiveInteger optimalSize;

	private PositiveInteger optimalSpeed;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public AttackSubsystemImpl(String name, PositiveInteger baseDamage, PositiveInteger optimalSize,
			PositiveInteger optimalSpeed, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		this.name = name;
		this.baseDamage = baseDamage;
		this.optimalSize = optimalSize;
		this.optimalSpeed = optimalSpeed;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {
		if (name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return AttackSubsystemBuilder.named(name).pg(powergridRequirments.value())
				.capacitorUsage(capacitorConsumption.value()).optimalSpeed(optimalSpeed.value())
				.optimalSize(optimalSize.value()).damage(baseDamage.value()).construct();
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		final int optimalSizeValue = this.optimalSize.value();
		final int targetSizeValue = target.getSize().value();
		double sizeReductionModifier = targetSizeValue >= optimalSizeValue ? 1
				: (double) targetSizeValue / optimalSizeValue;
		final var targetSpeed = target.getCurrentSpeed().value();
		final var optimalSpeedValue = this.optimalSpeed.value();
		double speedReductionModifier = targetSpeed <= optimalSpeedValue ? 1
				: (double) optimalSpeedValue / (2 * targetSpeed);
		int damage = (int) Math.ceil((this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier)));
		return PositiveInteger.of(damage);
	}

	@Override
	public String getName() {
		return this.name;
	}

}
