package com.binary_studio.tree_max_depth;

import java.util.Optional;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {

		if (rootDepartment == null) {
			return 0;
		}
		int deepest = 0;
		for (Department department : rootDepartment.subDepartments) {
			Optional<Department> child = Optional.ofNullable(department);
			Department newChild = child.orElse(null);
			deepest = Math.max(deepest, calculateMaxDepth(newChild));
		}
		return deepest + 1;
	}

}
